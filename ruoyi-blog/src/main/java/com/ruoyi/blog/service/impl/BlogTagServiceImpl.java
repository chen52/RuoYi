package com.ruoyi.blog.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.blog.mapper.BlogTagMapper;
import com.ruoyi.blog.domain.BlogTag;
import com.ruoyi.blog.service.IBlogTagService;
import com.ruoyi.common.core.text.Convert;

/**
 * Blog标签 服务层实现
 * 
 * @author ruoyi
 * @date 2019-05-07
 */
@Service
public class BlogTagServiceImpl implements IBlogTagService 
{
	@Autowired
	private BlogTagMapper blogTagMapper;

	/**
     * 查询Blog标签信息
     * 
     * @param tagId Blog标签ID
     * @return Blog标签信息
     */
    @Override
	public BlogTag selectBlogTagById(Integer tagId)
	{
	    return blogTagMapper.selectBlogTagById(tagId);
	}
	
	/**
     * 查询Blog标签列表
     * 
     * @param blogTag Blog标签信息
     * @return Blog标签集合
     */
	@Override
	public List<BlogTag> selectBlogTagList(BlogTag blogTag)
	{
	    return blogTagMapper.selectBlogTagList(blogTag);
	}
	
    /**
     * 新增Blog标签
     * 
     * @param blogTag Blog标签信息
     * @return 结果
     */
	@Override
	public int insertBlogTag(BlogTag blogTag)
	{
	    return blogTagMapper.insertBlogTag(blogTag);
	}
	
	/**
     * 修改Blog标签
     * 
     * @param blogTag Blog标签信息
     * @return 结果
     */
	@Override
	public int updateBlogTag(BlogTag blogTag)
	{
	    return blogTagMapper.updateBlogTag(blogTag);
	}

	/**
     * 删除Blog标签对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteBlogTagByIds(String ids)
	{
		return blogTagMapper.deleteBlogTagByIds(Convert.toStrArray(ids));
	}
	
}
