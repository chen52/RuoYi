package com.ruoyi.blog.service;

import com.ruoyi.blog.domain.BlogTag;
import java.util.List;

/**
 * Blog标签 服务层
 * 
 * @author ruoyi
 * @date 2019-05-07
 */
public interface IBlogTagService 
{
	/**
     * 查询Blog标签信息
     * 
     * @param tagId Blog标签ID
     * @return Blog标签信息
     */
	public BlogTag selectBlogTagById(Integer tagId);
	
	/**
     * 查询Blog标签列表
     * 
     * @param blogTag Blog标签信息
     * @return Blog标签集合
     */
	public List<BlogTag> selectBlogTagList(BlogTag blogTag);
	
	/**
     * 新增Blog标签
     * 
     * @param blogTag Blog标签信息
     * @return 结果
     */
	public int insertBlogTag(BlogTag blogTag);
	
	/**
     * 修改Blog标签
     * 
     * @param blogTag Blog标签信息
     * @return 结果
     */
	public int updateBlogTag(BlogTag blogTag);
		
	/**
     * 删除Blog标签信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteBlogTagByIds(String ids);
	
}
