package com.ruoyi.blog.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.blog.mapper.BlogMapper;
import com.ruoyi.blog.domain.Blog;
import com.ruoyi.blog.service.IBlogService;
import com.ruoyi.common.core.text.Convert;

/**
 * BLog文章 服务层实现
 * 
 * @author ruoyi
 * @date 2019-05-09
 */
@Service
public class BlogServiceImpl implements IBlogService 
{
	@Autowired
	private BlogMapper blogMapper;

	/**
     * 查询BLog文章信息
     * 
     * @param blogId BLog文章ID
     * @return BLog文章信息
     */
    @Override
	public Blog selectBlogById(Integer blogId)
	{
	    return blogMapper.selectBlogById(blogId);
	}
	
	/**
     * 查询BLog文章列表
     * 
     * @param blog BLog文章信息
     * @return BLog文章集合
     */
	@Override
	public List<Blog> selectBlogList(Blog blog)
	{
	    return blogMapper.selectBlogList(blog);
	}
	
    /**
     * 新增BLog文章
     * 
     * @param blog BLog文章信息
     * @return 结果
     */
	@Override
	public int insertBlog(Blog blog)
	{
	    return blogMapper.insertBlog(blog);
	}
	
	/**
     * 修改BLog文章
     * 
     * @param blog BLog文章信息
     * @return 结果
     */
	@Override
	public int updateBlog(Blog blog)
	{
	    return blogMapper.updateBlog(blog);
	}

	/**
     * 删除BLog文章对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteBlogByIds(String ids)
	{
		return blogMapper.deleteBlogByIds(Convert.toStrArray(ids));
	}
	
}
