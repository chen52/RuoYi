package com.ruoyi.blog.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.blog.mapper.BlogCategoryMapper;
import com.ruoyi.blog.domain.BlogCategory;
import com.ruoyi.blog.service.IBlogCategoryService;
import com.ruoyi.common.core.text.Convert;

/**
 * blog分类 服务层实现
 * 
 * @author ruoyi
 * @date 2019-05-06
 */
@Service
public class BlogCategoryServiceImpl implements IBlogCategoryService 
{
	@Autowired
	private BlogCategoryMapper blogCategoryMapper;

	/**
     * 查询blog分类信息
     * 
     * @param cateId blog分类ID
     * @return blog分类信息
     */
    @Override
	public BlogCategory selectBlogCategoryById(Integer cateId)
	{
	    return blogCategoryMapper.selectBlogCategoryById(cateId);
	}
	
	/**
     * 查询blog分类列表
     * 
     * @param blogCategory blog分类信息
     * @return blog分类集合
     */
	@Override
	public List<BlogCategory> selectBlogCategoryList(BlogCategory blogCategory)
	{
	    return blogCategoryMapper.selectBlogCategoryList(blogCategory);
	}
	
    /**
     * 新增blog分类
     * 
     * @param blogCategory blog分类信息
     * @return 结果
     */
	@Override
	public int insertBlogCategory(BlogCategory blogCategory)
	{
	    return blogCategoryMapper.insertBlogCategory(blogCategory);
	}
	
	/**
     * 修改blog分类
     * 
     * @param blogCategory blog分类信息
     * @return 结果
     */
	@Override
	public int updateBlogCategory(BlogCategory blogCategory)
	{
	    return blogCategoryMapper.updateBlogCategory(blogCategory);
	}

	/**
     * 删除blog分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteBlogCategoryByIds(String ids)
	{
		return blogCategoryMapper.deleteBlogCategoryByIds(Convert.toStrArray(ids));
	}
	
}
