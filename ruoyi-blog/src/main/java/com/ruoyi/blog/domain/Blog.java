package com.ruoyi.blog.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * BLog文章表 sys_blog
 * 
 * @author ruoyi
 * @date 2019-05-09
 */
public class Blog extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** BlogID */
	private Integer blogId;
	/** Blog标题 */
	private String blogTitle;
	/** 显示顺序 */
	private Integer blogSort;
	/** Blog分类ID */
	private Integer cateId;
	/** Blog文章内容 */
	private String blogContent;
	/** 数据范围（1：全部数据权限 2：自定数据权限） */
	private String dataScope;
	/** 标签状态（0正常 1停用） */
	private String status;
	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;

	public void setBlogId(Integer blogId) 
	{
		this.blogId = blogId;
	}

	public Integer getBlogId() 
	{
		return blogId;
	}
	public void setBlogTitle(String blogTitle) 
	{
		this.blogTitle = blogTitle;
	}

	public String getBlogTitle() 
	{
		return blogTitle;
	}
	public void setBlogSort(Integer blogSort) 
	{
		this.blogSort = blogSort;
	}

	public Integer getBlogSort() 
	{
		return blogSort;
	}
	public void setCateId(Integer cateId) 
	{
		this.cateId = cateId;
	}

	public Integer getCateId() 
	{
		return cateId;
	}
	public void setBlogContent(String blogContent) 
	{
		this.blogContent = blogContent;
	}

	public String getBlogContent() 
	{
		return blogContent;
	}
	public void setDataScope(String dataScope) 
	{
		this.dataScope = dataScope;
	}

	public String getDataScope() 
	{
		return dataScope;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setDelFlag(String delFlag) 
	{
		this.delFlag = delFlag;
	}

	public String getDelFlag() 
	{
		return delFlag;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("blogId", getBlogId())
            .append("blogTitle", getBlogTitle())
            .append("blogSort", getBlogSort())
            .append("cateId", getCateId())
            .append("blogContent", getBlogContent())
            .append("dataScope", getDataScope())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
