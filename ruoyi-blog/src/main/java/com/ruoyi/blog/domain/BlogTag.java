package com.ruoyi.blog.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * Blog标签表 sys_blog_tag
 * 
 * @author ruoyi
 * @date 2019-05-07
 */
public class BlogTag extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 标签ID */
	private Integer tagId;
	/** 标签名称 */
	private String tagName;
	/** 显示顺序 */
	private Integer roleSort;
	/** 数据范围（1：全部数据权限 2：自定数据权限） */
	private String dataScope;
	/** 标签状态（0正常 1停用） */
	private String status;
	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;

	public void setTagId(Integer tagId)
	{
		this.tagId = tagId;
	}

	public Integer getTagId()
	{
		return tagId;
	}
	public void setTagName(String tagName)
	{
		this.tagName = tagName;
	}

	public String getTagName()
	{
		return tagName;
	}
	public void setRoleSort(Integer roleSort) 
	{
		this.roleSort = roleSort;
	}

	public Integer getRoleSort() 
	{
		return roleSort;
	}
	public void setDataScope(String dataScope) 
	{
		this.dataScope = dataScope;
	}

	public String getDataScope() 
	{
		return dataScope;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setDelFlag(String delFlag) 
	{
		this.delFlag = delFlag;
	}

	public String getDelFlag() 
	{
		return delFlag;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tagId",getTagId())
            .append("tagName", getTagName())
            .append("roleSort", getRoleSort())
            .append("dataScope", getDataScope())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
