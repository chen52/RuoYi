package com.ruoyi.blog.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * blog分类表 sys_blog_category
 * 
 * @author ruoyi
 * @date 2019-05-06
 */
public class BlogCategory extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 分类ID */
	private Integer cateId;
	/** 分类名称 */
	private String cateName;
	/** 显示顺序 */
	private Integer roleSort;
	/** 数据范围（1：全部数据权限 2：自定数据权限） */
	private String dataScope;
	/** 分类状态（0正常 1停用） */
	private String status;
	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;

	public void setCateId(Integer cateId) 
	{
		this.cateId = cateId;
	}

	public Integer getCateId() 
	{
		return cateId;
	}
	public void setCateName(String cateName) 
	{
		this.cateName = cateName;
	}

	public String getCateName() 
	{
		return cateName;
	}
	public void setRoleSort(Integer roleSort) 
	{
		this.roleSort = roleSort;
	}

	public Integer getRoleSort() 
	{
		return roleSort;
	}
	public void setDataScope(String dataScope) 
	{
		this.dataScope = dataScope;
	}

	public String getDataScope() 
	{
		return dataScope;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setDelFlag(String delFlag) 
	{
		this.delFlag = delFlag;
	}

	public String getDelFlag() 
	{
		return delFlag;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cateId", getCateId())
            .append("cateName", getCateName())
            .append("roleSort", getRoleSort())
            .append("dataScope", getDataScope())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
