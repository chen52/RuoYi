package com.ruoyi.blog.mapper;

import com.ruoyi.blog.domain.BlogTag;
import java.util.List;	

/**
 * Blog标签 数据层
 * 
 * @author ruoyi
 * @date 2019-05-07
 */
public interface BlogTagMapper 
{
	/**
     * 查询Blog标签信息
     * 
     * @param tagId Blog标签ID
     * @return Blog标签信息
     */
	public BlogTag selectBlogTagById(Integer tagId);
	
	/**
     * 查询Blog标签列表
     * 
     * @param blogTag Blog标签信息
     * @return Blog标签集合
     */
	public List<BlogTag> selectBlogTagList(BlogTag blogTag);
	
	/**
     * 新增Blog标签
     * 
     * @param blogTag Blog标签信息
     * @return 结果
     */
	public int insertBlogTag(BlogTag blogTag);
	
	/**
     * 修改Blog标签
     * 
     * @param blogTag Blog标签信息
     * @return 结果
     */
	public int updateBlogTag(BlogTag blogTag);
	
	/**
     * 删除Blog标签
     * 
     * @param tagId Blog标签ID
     * @return 结果
     */
	public int deleteBlogTagById(Integer tagId);
	
	/**
     * 批量删除Blog标签
     * 
     * @param tagIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteBlogTagByIds(String[] tagIds);
	
}