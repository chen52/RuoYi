package com.ruoyi.blog.mapper;

import com.ruoyi.blog.domain.Blog;
import java.util.List;	

/**
 * BLog文章 数据层
 * 
 * @author ruoyi
 * @date 2019-05-09
 */
public interface BlogMapper 
{
	/**
     * 查询BLog文章信息
     * 
     * @param blogId BLog文章ID
     * @return BLog文章信息
     */
	public Blog selectBlogById(Integer blogId);
	
	/**
     * 查询BLog文章列表
     * 
     * @param blog BLog文章信息
     * @return BLog文章集合
     */
	public List<Blog> selectBlogList(Blog blog);
	
	/**
     * 新增BLog文章
     * 
     * @param blog BLog文章信息
     * @return 结果
     */
	public int insertBlog(Blog blog);
	
	/**
     * 修改BLog文章
     * 
     * @param blog BLog文章信息
     * @return 结果
     */
	public int updateBlog(Blog blog);
	
	/**
     * 删除BLog文章
     * 
     * @param blogId BLog文章ID
     * @return 结果
     */
	public int deleteBlogById(Integer blogId);
	
	/**
     * 批量删除BLog文章
     * 
     * @param blogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteBlogByIds(String[] blogIds);
	
}