package com.ruoyi.blog.mapper;

import com.ruoyi.blog.domain.BlogCategory;
import java.util.List;	

/**
 * blog分类 数据层
 * 
 * @author ruoyi
 * @date 2019-05-06
 */
public interface BlogCategoryMapper 
{
	/**
     * 查询blog分类信息
     * 
     * @param cateId blog分类ID
     * @return blog分类信息
     */
	public BlogCategory selectBlogCategoryById(Integer cateId);
	
	/**
     * 查询blog分类列表
     * 
     * @param blogCategory blog分类信息
     * @return blog分类集合
     */
	public List<BlogCategory> selectBlogCategoryList(BlogCategory blogCategory);
	
	/**
     * 新增blog分类
     * 
     * @param blogCategory blog分类信息
     * @return 结果
     */
	public int insertBlogCategory(BlogCategory blogCategory);
	
	/**
     * 修改blog分类
     * 
     * @param blogCategory blog分类信息
     * @return 结果
     */
	public int updateBlogCategory(BlogCategory blogCategory);
	
	/**
     * 删除blog分类
     * 
     * @param cateId blog分类ID
     * @return 结果
     */
	public int deleteBlogCategoryById(Integer cateId);
	
	/**
     * 批量删除blog分类
     * 
     * @param cateIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteBlogCategoryByIds(String[] cateIds);
	
}