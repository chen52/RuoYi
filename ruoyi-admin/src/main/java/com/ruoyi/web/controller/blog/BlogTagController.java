package com.ruoyi.web.controller.blog;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.blog.domain.BlogTag;
import com.ruoyi.blog.service.IBlogTagService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * Blog标签 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-05-07
 */
@Controller
@RequestMapping("/blog/blogTag")
public class BlogTagController extends BaseController
{
    private String prefix = "blog/blogTag";
	
	@Autowired
	private IBlogTagService blogTagService;
	
	@RequiresPermissions("blog:blogTag:view")
	@GetMapping()
	public String blogTag()
	{
	    return prefix + "/blogTag";
	}
	
	/**
	 * 查询Blog标签列表
	 */
	@RequiresPermissions("blog:blogTag:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(BlogTag blogTag)
	{
		startPage();
        List<BlogTag> list = blogTagService.selectBlogTagList(blogTag);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出Blog标签列表
	 */
	@RequiresPermissions("blog:blogTag:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BlogTag blogTag)
    {
    	List<BlogTag> list = blogTagService.selectBlogTagList(blogTag);
        ExcelUtil<BlogTag> util = new ExcelUtil<BlogTag>(BlogTag.class);
        return util.exportExcel(list, "blogTag");
    }
	
	/**
	 * 新增Blog标签
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存Blog标签
	 */
	@RequiresPermissions("blog:blogTag:add")
	@Log(title = "Blog标签", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(BlogTag blogTag)
	{		
		return toAjax(blogTagService.insertBlogTag(blogTag));
	}

	/**
	 * 修改Blog标签
	 */
	@GetMapping("/edit/{tagId}")
	public String edit(@PathVariable("tagId") Integer tagId, ModelMap mmap)
	{
		BlogTag blogTag = blogTagService.selectBlogTagById(tagId);
		mmap.put("blogTag", blogTag);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存Blog标签
	 */
	@RequiresPermissions("blog:blogTag:edit")
	@Log(title = "Blog标签", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(BlogTag blogTag)
	{		
		return toAjax(blogTagService.updateBlogTag(blogTag));
	}
	
	/**
	 * 删除Blog标签
	 */
	@RequiresPermissions("blog:blogTag:remove")
	@Log(title = "Blog标签", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(blogTagService.deleteBlogTagByIds(ids));
	}
	
}
