package com.ruoyi.web.controller.blog;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.blog.domain.Blog;
import com.ruoyi.blog.service.IBlogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * BLog文章 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-05-09
 */
@Controller
@RequestMapping("/blog/blog")
public class BlogController extends BaseController
{
    private String prefix = "blog/blog";
	
	@Autowired
	private IBlogService blogService;
	
	@RequiresPermissions("blog:blog:view")
	@GetMapping()
	public String blog()
	{
	    return prefix + "/blog";
	}
	
	/**
	 * 查询BLog文章列表
	 */
	@RequiresPermissions("blog:blog:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Blog blog)
	{
		startPage();
        List<Blog> list = blogService.selectBlogList(blog);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出BLog文章列表
	 */
	@RequiresPermissions("blog:blog:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Blog blog)
    {
    	List<Blog> list = blogService.selectBlogList(blog);
        ExcelUtil<Blog> util = new ExcelUtil<Blog>(Blog.class);
        return util.exportExcel(list, "blog");
    }
	
	/**
	 * 新增BLog文章
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存BLog文章
	 */
	@RequiresPermissions("blog:blog:add")
	@Log(title = "BLog文章", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Blog blog)
	{		
		return toAjax(blogService.insertBlog(blog));
	}

	/**
	 * 修改BLog文章
	 */
	@GetMapping("/edit/{blogId}")
	public String edit(@PathVariable("blogId") Integer blogId, ModelMap mmap)
	{
		Blog blog = blogService.selectBlogById(blogId);
		mmap.put("blog", blog);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存BLog文章
	 */
	@RequiresPermissions("blog:blog:edit")
	@Log(title = "BLog文章", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody	
	public AjaxResult editSave(Blog blog)
	{		
		return toAjax(blogService.updateBlog(blog));
	}
	
	/**
	 * 删除BLog文章
	 */
	@RequiresPermissions("blog:blog:remove")
	@Log(title = "BLog文章", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(blogService.deleteBlogByIds(ids));
	}
	
}
