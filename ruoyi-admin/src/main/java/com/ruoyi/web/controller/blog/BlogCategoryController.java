package com.ruoyi.web.controller.blog;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.blog.domain.BlogCategory;
import com.ruoyi.blog.service.IBlogCategoryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * blog分类 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-05-06
 */
@Controller
@RequestMapping("/blog/blogCategory")
public class BlogCategoryController extends BaseController
{
    private String prefix = "blog/blogCategory";
	
	@Autowired
	private IBlogCategoryService blogCategoryService;
	
	@RequiresPermissions("blog:blogCategory:view")
	@GetMapping()
	public String blogCategory()
	{
	    return prefix + "/blogCategory";
	}
	
	/**
	 * 查询blog分类列表
	 */
	@RequiresPermissions("blog:blogCategory:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(BlogCategory blogCategory)
	{
		startPage();
        List<BlogCategory> list = blogCategoryService.selectBlogCategoryList(blogCategory);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出blog分类列表
	 */
	@RequiresPermissions("blog:blogCategory:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BlogCategory blogCategory)
    {
    	List<BlogCategory> list = blogCategoryService.selectBlogCategoryList(blogCategory);
        ExcelUtil<BlogCategory> util = new ExcelUtil<BlogCategory>(BlogCategory.class);
        return util.exportExcel(list, "blogCategory");
    }
	
	/**
	 * 新增blog分类
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存blog分类
	 */
	@RequiresPermissions("blog:blogCategory:add")
	@Log(title = "blog分类", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(BlogCategory blogCategory)
	{		
		return toAjax(blogCategoryService.insertBlogCategory(blogCategory));
	}

	/**
	 * 修改blog分类
	 */
	@GetMapping("/edit/{cateId}")
	public String edit(@PathVariable("cateId") Integer cateId, ModelMap mmap)
	{
		BlogCategory blogCategory = blogCategoryService.selectBlogCategoryById(cateId);
		mmap.put("blogCategory", blogCategory);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存blog分类
	 */
	@RequiresPermissions("blog:blogCategory:edit")
	@Log(title = "blog分类", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(BlogCategory blogCategory)
	{		
		return toAjax(blogCategoryService.updateBlogCategory(blogCategory));
	}
	
	/**
	 * 删除blog分类
	 */
	@RequiresPermissions("blog:blogCategory:remove")
	@Log(title = "blog分类", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(blogCategoryService.deleteBlogCategoryByIds(ids));
	}
	
}
